from random import randint

name = input("Hi! What is your name? " )

i = 5

while i > 0:
    month = randint(1, 12)
    year = randint(1924, 2004)

    answer = input(f"{name} were you born in {month} / {year}? Yes or no? ")

    if answer == "yes":
        print("I knew it!")
        break
    elif answer == "no" and i == 1:
        print("I have better things to do. Good bye.")

    else:
        print("Drat! Lemme try again!")

    i -= 1
